﻿//bootstrap controller onto page
(function ($) {
    var expander = function (node) {
        var overlay = $(node);

        // The toggle is already added, don't do anything
        if (overlay.find(".overlay-expander--toggle").length > 0) {
            return;
        }
        
        var toggle = $("<a href class='btn overlay-expander--toggle'><i class='icon icon-navigation-left'></i></a>");

        // Event wiring
        toggle.on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();

            var expanded = overlay.hasClass("overlay-expander--expanded");
            overlay.toggleClass("overlay-expander--expanded", !expanded);
            toggle.find("i")
                .toggleClass("icon-navigation-left", expanded)
                .toggleClass("icon-navigation-right", !expanded);
        });

        // Add the expander
        overlay.append(toggle);
    };

    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            // Convert the NodeList to an array
            var nodes = Array.prototype.slice.call(mutation.addedNodes);
            nodes.forEach(function (node) {
                // Not all nodes have a class attribute
                if (node.className && node.className.indexOf("umb-overlay-right") !== -1) {
                    expander(node);
                } else if (node.className && node.className.indexOf("umb-modal") !== -1) {
					if( node.firstChild ) {
						if (node.firstChild.className && node.firstChild.className.indexOf("umb-overlay-right") !== -1) {
							expander(node.firstChild);
						}
					}
				}
            });
        });
    });

    // Scope the observer to the content pane
    observer.observe(document.querySelector("body"), { childList: true, subtree: true });
})(jQuery);